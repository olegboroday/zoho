import requests
import psycopg2
import psycopg2.extras
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
import functools
import xml.etree.ElementTree as ET
import jira.client
from jira.client import JIRA
from jira.client import GreenHopper

class ScriptCheck:
    def __init__(self):
        self.conn_string = 'host=softcubepostgre02.cloudapp.net dbname=softcube user=admin password=P@$$w0rd88'

    # get all urls from tenants
    def get_urls_main(self):
        pg_client = psycopg2.connect(self.conn_string)
        pg_cursor = pg_client.cursor(cursor_factory=psycopg2.extras.DictCursor)
        pg_cursor.execute("select divisiontitle from sc.tenant_division a where divisiontitle like '%.%' and divisiontitle not like '%Аптека%'/* and EXISTS (select * from sc.tenant b where b.id = a.tenant_id and status = 'opened')*/")
        rows = pg_cursor.fetchall()
        result = []
        for row in rows:
            result.append(row)
        return result

    # check if script if on site, if yes convert lead to account and create tasks to Jira
    def check_script (self, url, lead_id):
        driver = webdriver.Firefox()
        driver.get('http://www.' + url.replace("https://", "").replace("http://", "").lower().replace("www.", "")) #
        html_source = driver.page_source
        if "cdnanalytics.datasoftcube.com" not in str(html_source):
            text = "Script on {0} wasn`t found".format(url)
            driver.quit()
            return text
        else:
            l = Leads()
            l.convertLead(url, lead_id)
            j = Jira()
            j.create_story('Demo', url)
            j.create_story('Crawler', url)
            j.create_story('Tracking', url)
            print("{0} must be converted to account, {1}".format(url, lead_id))
        driver.quit()

    # send email
    def sendmail(self, text, to):
        # Create message container - the correct MIME type is multipart/alternative.
        msg = MIMEMultipart('alternative')
        msg['Subject'] = text
        msg['From'] = "form-website@softcube.com"
        msg['To'] = to

        # Record the MIME types of both parts - text/plain and text/html.
        part1 = MIMEText(text, 'plain')

        # Attach parts into message container.
        # According to RFC 2046, the last part of a multipart message, in this case
        # the HTML message, is best and preferred.
        msg.attach(part1)

        # Send the message via local SMTP server.
        s = smtplib.SMTP('smtp.gmail.com', port=587)
        s.ehlo()
        s.starttls()

        # sendmail function takes 3 arguments: sender's address, recipient's address
        # and message to send - here it is sent as one string.
        s.login("developer@softcube.com", "P@$$w0rd88")
        s.sendmail(msg['From'], msg['To'], msg.as_string())
        s.quit()

class Leads:
    def __init__(self):
        self.module_name = 'Leads'
        self.authtoken = '277d9fe3043b65d6f8df0fb526f215ee'
        self.user = 'a.pugach@softcube.com'
        self.password = '01f:7E8213'
        self.headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
        self.industry = ['Fashion & Footwear','Electronics','Books & Multimedia','Booking','Food','Toys & Babycare','DIY & Householding','Beauty & Healthcare','Gifts & Flowers','Hobbies', 'Pet Supplies']

    # get lead data (Id and WebSite) from Zoho
    def get_leads(self):
        url = "https://crm.zoho.com/crm/private/xml/"+self.module_name+"/getRecords"
        params = {'authtoken':self.authtoken, 'scope':'crmapi', 'newFormat':2, 'fromIndex':1, 'toIndex':200}
        r = requests.get(url, params=params)
        root = ET.fromstring(r.text)
        result = []
        for child in root:
            for child2 in child:
                for child3 in child2:
                    for child4 in child3:
                        if child4.attrib['val'] == 'LEADID':
                            id = child4.text
                        if child4.attrib['val'] == 'Website':
                            site = child4.text
                        if child4.attrib['val'] == 'Industry':
                            ind = child4.text
                        if child4.attrib['val'] == 'Rating':
                            rat = child4.text
                            if rat == 'General Flow' and ind in self.industry:
                                result.append((id,site))
        return result;

    # convert Lead to Account by lead_id
    def convertLead(self, url, lead_id):

        account_owner = 'bogdan.golikov@softcube.com'

        xmlData = '<Accounts>'
        xmlData += '<row no="1">'
        xmlData += '<option val="createAccount">true</option>'
        xmlData += '<option val="assignTo">{0}</option>'.format(account_owner)
        xmlData += '<option val="notifyLeadOwner">true</option>'
        xmlData += '<option val="notifyNewEntityOwner">true</option>'
        xmlData += '</row>'
        xmlData += '<row no="2">'
        xmlData += '<FL val="Account Name">{0}</FL>'.format(url)
        #xmlData += '<FL val="Closing Date">12/21/2009</FL><FL val="Potential Stage">Closed Won</FL>'
        #xmlData += '<FL val="Contact Role">Purchasing</FL>'
        #xmlData += '<FL val="Amount">3432.23</FL>'
        #xmlData += '<FL val="Probability">100</FL>'
        xmlData += '</row>'
        xmlData += '</Accounts>'

        params = {'authtoken':self.authtoken,'scope':'crmapi', 'leadId':lead_id, 'xmlData':xmlData}

        r = requests.post('https://crm.zoho.com/crm/private/xml/Leads/convertLead', params = params)
        print(r.url)

class Jira:

    def __init__(self):
        self.user = 'alex.pugach'
        self.password = '66m36jdqv2ej'
        self.options = {'server': 'http://jira.softcube.com'}

    # create task in Jira
    def create_story(self, type, client):
        jira = JIRA(self.options, basic_auth=(self.user, self.password))
        summary = 'Create {0} for {1}'.format(type,client)
        if type == 'Crawler' or type == 'Demo':
            assignee = {'name':'ivan.doyar'}
        else:
            assignee = {'name':'alex.mushtaev'}

        new_issue = jira.create_issue(project={'key': 'SC'}, summary=summary, description=summary, issuetype={'name': 'Story'}, assignee=assignee)

if __name__ == '__main__':
    s = ScriptCheck()
    urls = s.get_urls_main()
    l = Leads()
    leads = l.get_leads()

    urls_check = []
    for l in leads:
        for u in urls:
            if 'http://www.' + l[1].replace("https://", "").replace("http://", "").lower().replace("www.", "").replace("/", "") == 'http://www.' + u[0].replace("https://", "").replace("http://", "").lower().replace("www.", "").replace("/", ""):
                urls_check.append((l[0], l[1]))

    result = []
    for u in urls_check:
        text = s.check_script(u[1], u[0])
        result.append(text)

    email_text = "\n".join(result)
    recipients = ["bogdan.golikov@softcube.com", "roman.zakharov@softcube.com"]
    [s.sendmail(email_text, r) for r in recipients]


